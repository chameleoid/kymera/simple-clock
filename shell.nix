with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "clock-env";

  buildInputs = [
    nodejs-8_x
    (yarn.override { nodejs = nodejs-8_x; })
  ];

  shellHook = ''
    export PATH="$HOME/.yarn/bin:$PWD/node_modules/.bin:$PATH"
    export NODE_ENV=development
  '';
}
