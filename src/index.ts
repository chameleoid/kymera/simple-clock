import PigpioClient from 'pigpio-client';
import liblouis from 'liblouis';

liblouis.enableOnDemandTableLoading('node_modules/liblouis-js/tables/');

interface DisplayOptions {
  pins: PigpioClient.Gpio[];
}

class Display {
  private pins: PigpioClient.Gpio[];
  private tables: string = 'tables/unicode.dis,tables/en-us-comp6.ctb';

  constructor(options: DisplayOptions) {
    this.pins = options.pins;
  }

  private codepointBitArray(codepoint: string): number[] {
    let code: number = codepoint.charCodeAt(0) - 0x2800;
    let out: number[] = new Array(8);

    for (let bit: number = 0; bit < 8; bit++, code >>= 1) {
      out[bit] = code & 1;
    }

    return out;
  }

  async write(text: string): Promise<void> {
    let translated: string = liblouis.translateString(this.tables, text);
    let self: this = this;

    await new Promise((resolve: Function) => {
      (function next(point: number = 0): void {
        self.codepointBitArray(translated[point])
          .map((bit, i) => self.pins[i].write(bit));

        if (++point < translated.length) {
          setTimeout(next, 500, point);
        } else {
          resolve();
        }

        setTimeout(self.clear.bind(self), 400);
      })();
    });
  }

  async clear(): Promise<void> {
    await Promise.all(this.pins.map(async point => await point.write(0)));
  }
}

const pi = new PigpioClient.pigpio({
  host: process.env.NODE_ENV == 'development' ? '10.42.0.123' : 'localhost',
  port: 8888,
  pipelining: true,
});

pi.on('connected', (info: PigpioClient.Info) => {
  console.log('Connected to', info.host);

  let display = new Display({
    pins: [
      pi.gpio(18),
      pi.gpio(20),
      pi.gpio(22),

      pi.gpio(19),
      pi.gpio(21),
      pi.gpio(23),

      pi.gpio(24),
      pi.gpio(25),
    ],
  });

  /*let step = () => leds.map(led => {
    if (++led.pwm >= 255)
      led.pwm = -255;

    led.pin.analogWrite(Math.round(Math.pow(255, Math.abs(led.pwm) / 255)));
  });
  
  setInterval(step, 20);*/

  (async function getTime(): Promise<void> {
    let date: Date = new Date();
    let time: string = date.toTimeString().substr(0, 5);

    await display.write(time);
    setTimeout(getTime, 60 * 1000);
  })();

  process.on('SIGINT', async () => {
    await display.clear();
    process.exit();
  });
});
