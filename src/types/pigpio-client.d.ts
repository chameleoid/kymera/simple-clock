declare module 'pigpio-client' {
  class pigpio {
    constructor(pi: Info);

    gpio(pin: number): Gpio;

    on(event: 'connected', listener: (info: Info) => void): void;
  }

  interface Gpio {
    pwm: number;
    write(level: number, callback?: Function): void;
  }

  interface Info {
    host?: string;
    port?: number;
    pipelining?: boolean;
    commandSocket?: boolean;
    notificationSocket?: boolean;
    pigpioVersion?: string;
    hwVersion?: string;
    hardware_type?: number;
    userGpioMask?: number;
  }
}
